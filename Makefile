CC=gcc

build:
	$(CC) -g -c queue.c -o queue.o	

test: build
	$(CC) -g -Wall -Werror -pedantic -o test unittest.c queue.o 
