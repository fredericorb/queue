#include "queue.h"

const int SUCCESS = 0;
const int QUEUE_MAX_NUMBERS_REACHED = 1;
const int INVALID_QUEUE_PROVIDED = 2;

queue_t* q_create (size_t max_elements) {
	if (max_elements < 1)
		return NULL;
	queue_t* q = malloc(sizeof(queue_t));
	if (q != NULL) {
		q->max_elements = max_elements + 1;
		q->head = 0; 
		q->tail = 0;
		q->memory = malloc(sizeof(void*) * max_elements);
	}
	return q;
}

int q_enqueue (queue_t* q, void* item) {
	if (q == NULL) 
		return INVALID_QUEUE_PROVIDED;
	if ((q->tail + 1) % q->max_elements == q->head)
		return QUEUE_MAX_NUMBERS_REACHED;
	q->memory[q->tail] = item;
	q->tail = (q->tail + 1) % q->max_elements;
	return SUCCESS;
}

void* q_dequeue (queue_t* q) {
	if (q == NULL || q->head == q->tail) 
		return NULL;
	void* value = q->memory[q->head];
	q->head = (q->head + 1) % q->max_elements;
	return value;
}

