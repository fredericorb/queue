#ifndef QUEUE_H
#define QUEUE_H

#include<stdlib.h>

typedef struct {
	void** memory;
	size_t max_elements;
	size_t head, tail;
} queue_t;

queue_t* q_create (size_t max_elements);

int q_enqueue (queue_t* q, void* item);

void* q_dequeue (queue_t* q);

#endif 
